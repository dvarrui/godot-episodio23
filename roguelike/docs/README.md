
# Roguelike

Enlaces de interés:
* Arte: https://o-lobster.itch.io/simple-dungeon-crawler-16x16-pixel-pack
* StateMachine game endevour: https://github.com/MateuSai/Godot-Roguelike-Tutorial
* GitHub tutorial code: https://github.com/MateuSai/Godot-Roguelike-Tutorial/tree/main

Vídeos
0. Settings:
1. Character: https://www.youtube.com/watch?v=PcbO6aJKGic
2. Player attack: https://www.youtube.com/watch?v=jYCY81gShSI
3. Enemy path finding: https://www.youtube.com/watch?v=x8FVpN9r2Bo
4. HitBox: https://www.youtube.com/watch?v=Y6Cicwz2pN4
5. Player dead: https://www.youtube.com/watch?v=H6mDX1678SU
6. Health bar: https://youtu.be/c7n5ocmBiLw6

# ANEXO

* [Los 12 principios de animación](https://www.tesseractspace.com/blog/los-12-principios-de-animacion/)

Ejemplo de animación:
* https://twitter.com/johanpeitz/status/1689691876310294528?t=Be1Vm2lb0Z9nURJwNiX1LA&s=35
* https://twitter.com/penusbmic/status/1692512744744853766
* https://twitter.com/Atticus__Finn/status/1691838615498735919?t=nmX9MaftCs6w72WZEzjirw&s=35

Ejemplo de imagen:
* Sci-Fi https://pbs.twimg.com/media/F30ETMaWAAAB-Q7?format=png&name=small
* Sci-Fi https://twitter.com/IpgNeilPar/status/1692242883963764906/photo/3
* Roguelike https://twitter.com/Atticus__Finn/status/1682058157005086722?t=MdFmi6pS6WWvubg9wmvySw&s=35
* Roguelike https://twitter.com/_V3X3D/status/1691468930949201921?t=egEZrJFybCLcqSNswFqojg&s=35
* Roguelike https://img.itch.zone/aW1hZ2UvMTE2NDQ4Mi83NDA2NTE2LnBuZw==/original/%2FmVJKr.png
