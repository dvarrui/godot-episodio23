extends CharacterBody2D

@onready var anim = $anim
var speed = Vector2(35,40)
var state = "idle"

func _ready():
	state = "idle"
	anim.play(state)
	self.z_index = self.position.y

