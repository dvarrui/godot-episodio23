extends CharacterBody2D

@onready var anim = $anim
var speed = Vector2(35,40)
var state = "idle"

func _ready():
	state = "idle"
	anim.play(state)

func _physics_process(delta):
	self.z_index = self.position.y
	var new_state = state
	var dir = Vector2.ZERO
	if Input.is_action_pressed("player_up"):
		state = "walk_front"
		dir.y = -1
	if Input.is_action_pressed("player_down"):
		state = "walk_front"
		dir.y = 1
	if Input.is_action_pressed("player_left"):
		state = "walk_left"
		dir.x = -1
	if Input.is_action_pressed("player_right"):
		state = "walk_right"
		dir.x = 1
	if dir == Vector2.ZERO:
		state = "idle"
	if new_state != state:
		anim.play(state)
	var norm = dir.normalized()
	velocity = Vector2(norm.x * speed.x, norm.y * speed.y)
	move_and_slide()
